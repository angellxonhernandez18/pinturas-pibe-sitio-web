// Importa Firebase
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore'; // Si lo necesitas
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// Configura Firebase
// Import the functions you need from the SDKs you need
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBUOIBRYos_p3C7bt6n7CDbq0j2_i9o1I4",
  authDomain: "pinturas-pibe.firebaseapp.com",
  databaseURL: "https://pinturas-pibe-default-rtdb.firebaseio.com",
  projectId: "pinturas-pibe",
  storageBucket: "pinturas-pibe.appspot.com",
  messagingSenderId: "913642298366",
  appId: "1:913642298366:web:05cf0c8349bf7850f1feaf",
  measurementId: "G-108797Z5B5"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
//---------------------------------------------------------------

// Autenticación de Firebase
const auth = firebase.auth();

// Iniciar sesión
auth.signInWithEmailAndPassword('correo@example.com', 'contraseña')
  .then((userCredential) => {
    // Usuario autenticado con éxito
    const user = userCredential.user;
  })
  .catch((error) => {
    // Manejo de errores
    console.error(error.message);
  });


  