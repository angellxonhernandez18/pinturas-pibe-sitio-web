document.getElementById('loginForm').addEventListener('submit', function(event) {
  event.preventDefault();  // Prevent form submission

  // Get the username and password from the form
  const username = document.getElementById('username').value;
  const password = document.getElementById('password').value;

  // Simulate authentication (replace this with your authentication logic)
  const isAuthenticated = authenticate(username, password);

  if (isAuthenticated) {
    // Redirect to the home page
    window.location.href = 'administrador.html';
  } else {
    alert('Invalid username or password. Please try again.');
  }
});

function authenticate(username, password) {
  // Simulated authentication logic (replace this with your authentication logic)
  return username === 'admin' && password === 'admin';
}
